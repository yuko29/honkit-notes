# 80x86 Port

###### tags: `RTOS` `uC/OS`

> Adapting a real-time kernel to a microprocessor or a microcontroller is called a *port*.

Port 指的是使一個 real-time kernel 適應特定微處理器的行為。

多數是和處理器設計結構有關的部份，例如 register 大小、memory address 定義等等。

需要處理的部份主要有三：

1. Stack initialization
2. Context switch
3. Timer ISR

因為跟硬體設計相關，port 相關的 code 都以 assembly 實作。

## x86 Real Mode

Intel 80286 有兩種操作模式： Real Mode 和 Protected Mode。
Real Mode 是 80286 和之後的 x86 相容 CPU 的操作模式。

而 Virtual 86 mode (virtual real mode) 是 Intel 80386 後的一種操作模式，允許在 protected mode OS 中執行 real mode 的程式。

特色：

* 16 bit 的 program
* 20 bit 的區段記憶體位址空間（1MB 的記憶體空間可以被定址）

![](https://i.imgur.com/xSFloYr.png)

![](https://i.imgur.com/KtJXO9d.png)


## Stack Initialization

一個新建立的 task 是一個 ready task。
所有的 ready task 都是處在被中斷且正要離開 ISR 的狀態。
於是，需要在新 task 的 task stack context 必須模仿 (emulate) interruped task 的 stack context，存放 CPU content，使新 task 要被執行時，可照用 context switch 的方式 "restore" CPU content。

```
OS_STK *OSTaskStkInit (void  (*task)(void *pd),
                       void   *pdata,
                       OS_STK *ptos,
                       INT16U  opt);
{
    Simulate call to function with an argument (i.e., pdata);               (1)
    Simulate ISR vector;                                                    (2)
    Setup stack frame to contain desired initial values of all registers;   (3)
    Return new top-of-stack pointer to caller;                              (4)
}
```

![](https://i.imgur.com/6pU0LJK.png)

## Context Switch

### OSStartHighRdy()

```
void OSStartHighRdy (void)
{
    Call user definable OSTaskSwHook();                                  
    OSRunning = TRUE;                                                
    Get the stack pointer of the task to resume:                          
        Stack pointer = OSTCBHighRdy->OSTCBStkPtr;
  
    Restore all processor registers from the new task's stack;         
    Execute a return from interrupt instruction;                       
}
void OSStartHighRdy (void)
```

![](https://i.imgur.com/XFkLoQC.png)

### OSCtxSw()

Task-level context switch

> This function is called to perform a task level context switch. Generally, this function is invoked via a software interrupt instruction (also called a TRAP instruction). The pseudocode for this function is shown below.

> OSCtxSw is installed at ISR 0x80.

> Called from OS_TASK_SW()


```
void  OSCtxSw (void)
{
    Save processor registers;
    Save the current tasks stack pointer into the current tasks OS_TCB:
        OSTCBCur->OSTCBStkPtr = Stack pointer;
    Call user definable OSTaskSwHook();                                 
    OSTCBCur  = OSTCBHighRdy;                                           
    OSPrioCur = OSPrioHighRdy;                                          
    Get the stack pointer of the task to resume:                        
        Stack pointer = OSTCBHighRdy->OSTCBStkPtr;
    Restore all processor registers from the new tasks stack;          
    Execute a return from interrupt instruction;                        
}
```

### OSIntCtxSw()

Interrupt-level context switch

> Called from OSIntExit to perform a context switch when returning from an ISR
> 所有的 register 包含 SS:SP 在此之前都已經被儲存至 stack 
```
void  OSIntCtxSw (void)
{
    Call user-definable OSTaskSwHook();
    OSTCBCur  = OSTCBHighRdy;
    OSPrioCur = OSPrioHighRdy;
    Get the stack pointer of the task to resume:
        Stack pointer = OSTCBHighRdy->OSTCBStkPtr;
    Restore all processor registers from the new tasks stack;
    Execute a return from interrupt instruction;
}
```

### OSTickISR()

第一個 task 需要負責 install OSTickISR


![](https://i.imgur.com/UxiWpNj.png)
![](https://i.imgur.com/W9osvBp.png)
