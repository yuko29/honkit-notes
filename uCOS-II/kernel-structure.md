# Kernel Structure

###### tags: `RTOS` `uC/OS`

![](https://i.imgur.com/ZePjIoW.png)

---

## Critical seciton

一般在 user space 會用 semaphore 或 mutex 來保護 critical seciton，但這種作法在 kernel space 並不適用：
- kernel critical section 通常較短
- 在 kernel space 不期望甚至不允許 context switch 

#### Task-task race in kernel

kernel 內的 critical section 通常較短，semaphore 和 mutex 的 overhead 太大

#### Task-ISR race

ISR 不能呼叫 blocking calls
* 可能產生 deadlock
* 對 interrupted task 會產生 long delay 


---

解法：在 kernel 中，使用 **disable interrupt** 的方式，阻止 task preemption，藉此製造 critical section（此法只能用於 single processor）

* Disable interrupt 的時間必須盡量短
* 不可以在 disable interrupt 的時候 (或 ISR 裡) 呼叫 system service

> e.g., 如果在 disable interrupt 的時候呼叫 `OSTimeDly()`，因為 `OSTimeDly()` 需要等 TimeTick 的 interrupt，但 interrupt 被阻擋了，造成 deadlock 
```c
{
    .
    OS_ENTER_CRITICAL();    /* Disable interrupt */
        /* Critical Section*/
    OS_EXIT_CRITICAL()；     /* Enable interrupt */
}
```

`OS_ENTER_CRITICAL()`/`OS_EXIT_CRITICAL()` 實作方法有幾種:
- OS_CRITICAL_METHOD=1
    - 最直接的，在 `OS_ENTER_CRITICAL()` disable interrupt，在`OS_EXIT_CRITICAL()` enable interrupt
    - 過程不包含 task stack
- OS_CRITICAL_METHOD=2
    - 把 Process Status Word (PSW) 存到 stack 上，critical section 結束的時候 pop 出來 restore 先前 task 的 status
        - PSW 包含 condition code register, processor flag, etc
        - 每個 task 執行時 interrupt 要是 enable/disable 的狀態可以保留下來
            - `OS_EXIT_CRITICAL()` 結束後，之前執行狀態該是 disable interrupt 還是 enable interrupt 就回歸該狀態
    - 用於 nesting interrupt
    - x86 port 用此方法
```c
#define OS_ENTER_CRITICAL()   \
        asm(" PUSH   PSW")    \
        asm(" DI")
#define OS_EXIT_CRITICAL()    \
        asm(" POP    PSW")
```
> 為何需要 method 2？
> `OS_EXIT_CRITICAL()` 被呼叫後 interrupt 會立刻被 enable，如果到下一次 syscall 之前這段時間預期 interrupt 要是 disable 的話就會出問題。
> ```
> {
>     .
>     diable_interrupt();
>     /* Interrupt are now implictly re-enabled */  <--
>     a_kernel_service(); /* disable interrupt */
> }
> ```

---

## Task

> A task is an active entity that conduct computation.
> In real-time system, a periodic task is typically an infinite loop.

Task 是在系統中執行計算任務的實體。在 real-time system 中，一個 periodic task 是一個 infinite loop。

```
void YourTask (void *pdata)                                            (1)
{
   for (;;) {                                                          (2)
      /* USER CODE */
      Call one of uC/OS-II's services:
      OSFlagPend();
      OSMboxPend();
      OSMutexPend();
      OSQPend();
      OSSemPend();
      OSTaskDel(OS_PRIO_SELF);
      OSTaskSuspend(OS_PRIO_SELF);
      OSTimeDly();
      OSTimeDlyHMSM();
      /* USER CODE */
   }
}
```
* uC/OS-II 有 64 個 unique priorites
* 每個 Task 會被賦予一個 unique priorites
* Priorites 63 和 62 被保留給 idle 和 stat
* 通常 embbedded system 上的 task 不會太多，所以使用 unique priority 是可行的

### Idle task

idle task 是 priority 最低的 task。
```diff
void  OS_TaskIdle (void *pdata)
{
#if OS_CRITICAL_METHOD == 3                     
    OS_CPU_SR  cpu_sr;
#endif   
     
     
    pdata = pdata;                              
    for (;;) {
        OS_ENTER_CRITICAL();
+        OSIdleCtr++;                                            (1)
        OS_EXIT_CRITICAL();
+        OSTaskIdleHook();                                       (2)
    }
}
```
`OSTaskIdleHook()` 內可以放 user 想讓他做的事，不過 idle task 必須隨時保持 ready 的狀態，不能 call 會任何 PEND funtion (`OSTimeDly`) 或 suspend services。

### Task State

* Dormant: task 尚未被建立
* Ready: task 可被排程，此時 task 不處於 delay 狀態
* Running: task 正在被執行
* Waiting: task 被 delay
* ISR: task 執行中被 interrupt 插入，CPU 轉執行 ISR，task 此時等待 ISR 結束

![](https://i.imgur.com/iN139xz.png)


### Task Control Block (TCB)

每個 Task 都有一個 TCB。

在系統初始化的時候會先建立好所有可用的 TCB （64 個 TCB），如果 memory 不足的話系統會 crash。

* 在被使用的 TCB 會被放到 TCB list
* 沒被使用的 TCB 會被放到 free list

```cpp
typedef struct os_tcb {
     OS_STK        *OSTCBStkPtr;
  
 #if OS_TASK_CREATE_EXT_EN > 0
     void          *OSTCBExtPtr;
     OS_STK        *OSTCBStkBottom;
     INT32U         OSTCBStkSize; 
     INT16U         OSTCBOpt;     
     INT16U         OSTCBId;      
 #endif
  
     struct os_tcb *OSTCBNext;    
     struct os_tcb *OSTCBPrev;    
  
 #if OS_TASK_CREATE_EXT_EN > 0u
 #if defined(OS_TLS_TBL_SIZE) && (OS_TLS_TBL_SIZE > 0u)
     OS_TLS           OSTCBTLSTbl[OS_TLS_TBL_SIZE];
 #endif
 #endif
 
 #if ((OS_Q_EN > 0u) && (OS_MAX_QS > 0u)) || (OS_MBOX_EN > 0u) || (OS_SEM_EN > 0u) || (OS_MUTEX_EN > 0u)
     OS_EVENT      *OSTCBEventPtr;
 #endif
  
 #if (OS_EVENT_EN) && (OS_EVENT_MULTI_EN > 0u)
     OS_EVENT     **OSTCBEventMultiPtr;
 #endif
  
 #if ((OS_Q_EN > 0) && (OS_MAX_QS > 0)) || (OS_MBOX_EN > 0)
     void          *OSTCBMsg;  
 #endif
  
 #if (OS_FLAG_EN > 0u) && (OS_MAX_FLAGS > 0u)
 #if OS_TASK_DEL_EN > 0
     OS_FLAG_NODE  *OSTCBFlagNode; 
 #endif   
     OS_FLAGS       OSTCBFlagsRdy; 
 #endif
  
     INT16U         OSTCBDly;      
     INT8U          OSTCBStat;     
     INT8U          OSTCBStatPend;
     INT8U          OSTCBPrio;     
  
     INT8U          OSTCBX;        
     INT8U          OSTCBY;  
     INT8U          OSTCBBitX;     
     INT8U          OSTCBBitY;     
  
 #if OS_TASK_DEL_EN > 0u
     BOOLEAN OSTCBDelReq;   
 #endif
  
#if OS_TASK_PROFILE_EN > 0u
    INT32U           OSTCBCtxSwCtr;        
    INT32U           OSTCBCyclesTot;       
    INT32U           OSTCBCyclesStart;     
    OS_STK          *OSTCBStkBase;         
    INT32U           OSTCBStkUsed;         
#endif
#if OS_TASK_NAME_EN > 0u
    INT8U           *OSTCBTaskName;
#endif
 
#if OS_TASK_REG_TBL_SIZE > 0u
    INT32U           OSTCBRegTbl[OS_TASK_REG_TBL_SIZE];
#endif
 
 
 } OS_TCB;
```
## Context Switch and Scheduling

### Ready List

Ready List 實際上是以 bitmap 實作。
用途是查找 ready task 中 priority 最高的 task，只需要 O(1) 的搜尋時間。

![](https://i.imgur.com/lPvXUPa.png)

```
y    = OSUnMapTbl[OSRdyGrp];    /* Determine Y position in OSRdyTbl[]  */
x    = OSUnMapTbl[OSRdyTbl[y]]; /* Determine X position in OSRdyTbl[Y] */
prio = (y << 3) + x;
```

### Task Scheduling

Scheduler 會一直挑 highest-priority read task 給 CPU 執行，這個行為稱為 schedule。

schedule 在兩種情況下會發生：
* Task-level scheduling: `OS_Sched()`
    * 當 running task 主動放棄 CPU 執行權時
* ISR-level scheduling: `OSIntExit()`
    * 當 running task 被 preempt 時

#### Task-level Task Scheduling

Context switch 過程中會把所有的 CPU register 和 low priority task (LPT) 的 PSW 存到 LPT 的 stack，並從 high prioriy task（HPT）的 stack 把 CPU register 和 PSW 加載回來。

> 自己 task 的 CPU state 由自己的 task 的 stack 保管


當 task 要放棄 CPU 時，可呼叫 `OS_Sched()` (`OSTimeDly()` 也行，裡面會呼叫 `OS_Sched()`)，`OS_Sched()` 裡最後會呼叫 `OS_TASK_SW()`
`OS_TASK_SW()` 在 uC/OS-II 是一個 macro ，調用 software interrupt，在 x86 porting 會被展開成 `INT 80h`，CPU 之後就會執行 context switch。

> context switch 過程 以 assembly 實作
> * For efficiency
> * For direct access to register and stack

```diff
void  OS_Sched (void)
{
#if OS_CRITICAL_METHOD == 3
    OS_CPU_SR  cpu_sr;
#endif   
    INT8U      y;
  
  
    OS_ENTER_CRITICAL();
+    if ((OSIntNesting == 0u) && (OSLockNesting == 0u)) {                   (1)
        y             = OSUnMapTbl[OSRdyGrp];                              (2)
        OSPrioHighRdy = (INT8U)((y << 3) + OSUnMapTbl[OSRdyTbl[y]]);
        if (OSPrioHighRdy != OSPrioCur) {                                  (3)
            OSTCBHighRdy = OSTCBPrioTbl[OSPrioHighRdy];                    (4)
#if OS_TASK_PROFILE_EN > 0u
            OSTCBHighRdy->OSTCBCtxSwCtr++;         /* Inc. # of context switches to this task      */
#endif
            OSCtxSwCtr++;                                                  (5)
#if OS_TASK_CREATE_EXT_EN > 0u
#if defined(OS_TLS_TBL_SIZE) && (OS_TLS_TBL_SIZE > 0u)
            OS_TLS_TaskSw();
#endif
#endif
+            OS_TASK_SW();                                                  (6)
        }
    }
    OS_EXIT_CRITICAL();
}
```

### Interrupt

每個 tick 所觸發的 hardware interrupt，都是一個 schedule point。

tick 打進來後 CPU 會執行 clock tick ISR，在 uC/OS-II 稱為 `OSTickISR()`。

#### Clock Tick

```diff
void OSTickISR(void)
{
+    Save processor registers;
    Call OSIntEnter() or increment OSIntNesting;
    if (OSIntNesting == 1u) {
        OSTCBCur->OSTCBStkPtr = SP;
    }
+    Call OSTimeTick();                                             (1)
    Clear interrupting device;
    Re-enable interrupts (optional);
+    Call OSIntExit();
    Restore processor registers;
    Execute a return from interrupt instruction;
}
```
* `OSTimeTick()` 是 hardware independent 的，負責把 delay 中的 task 的 delay counter 減一，並把 counter 為 0 的 task 標示 ready task
* `OSTickISR()` 本身是 hardware dependent 的，save register 等等動作與硬體相關

> 線性的訪問所有 TCB 來 decrement delay
> * O(n) to progress 1 unit of time
> * O(1) insert a new sleeping task
> 
> Alternative: delta list (used in linux kernel)
> * O(1) to progress 1 unit of time
> * O(n) insert a new sleeping task

#### Interrupt-level Task Scheduling

在 `OSTickISR()` 結束前會執行 `OSIntExit()`，此為 schedule point

```diff
void  OSIntExit (void)
{
#if OS_CRITICAL_METHOD == 3                           
    OS_CPU_SR  cpu_sr;
#endif
     
     
    OS_ENTER_CRITICAL();
    if (OSRunning == TRUE) {
+        if (OSIntNesting > 0u) {                                      (1)
+            OSIntNesting--;
+        }
        if ((OSIntNesting == 0u) && (OSLockNesting == 0u)) {          
            OS_SchedNew();                                            (2)
            OSTCBHighRdy = OSTCBPrioTbl[OSPrioHighRdy];
            if (OSPrioHighRdy != OSPrioCur) {         
#if OS_TASK_PROFILE_EN > 0u
                OSTCBHighRdy->OSTCBCtxSwCtr++;        
#endif
                OSCtxSwCtr++;                         
#if OS_TASK_CREATE_EXT_EN > 0u
#if defined(OS_TLS_TBL_SIZE) && (OS_TLS_TBL_SIZE > 0u)
                OS_TLS_TaskSw();
#endif
#endif
 
+                OSIntCtxSw();                                        (3)
            }
        }
    }
    OS_EXIT_CRITICAL();
}
```

:::info
與 `OS_Sched()` 不同的地方：
1. `OS_Sched()` 不能在 ISR 內被呼叫 (OSIntNesting > 0u 會直接退出)
2. `OSIntExit()` 呼叫 `OSIntCtxSw()` 而非 `OS_TASK_SW()`
    * `OS_TASK_SW()` 會 save processor registers
    * 在 Clock tick ISR 中已經做了 save processor registers，所以這裡不用再做一次
:::

### Race Avoidance

* `OS_ENTER_CRITICAL` / `OS_EXIT_CRITICAL`
    * 製造 critical section
    * 在這之中不會發生 preemption 或 interrupt
* `OSSchedLock()` / `OSSchedUlock()`
    * 期間不執行 task rescheduling，維持當下的 task 執行，不論是否有更高 priority 的 task 在 ready state
    * 用於防止 task-task race condition
    * 期間不會發生 preemption，但允許 interrupt
    * 期間不能呼叫任何可能當下 task 被 pending 的 kernel serive，不然 system 會被 lock 住
    * 所有的 task 都會被影響
* `OSSemPend()` / `OSSemPost()`
    * 期間允許 preemption 和 interrupt
    * 只有 pending / posting task 會被影響
