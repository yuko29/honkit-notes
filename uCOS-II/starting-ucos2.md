# Starting uC/OS-II

###### tags: `RTOS` `uC/OS`

以下講述 uC/OS-II 如何啟動的。

```
void main (void)
{
    OSInit();           /* Initialize uC/OS-II                            */
    .
    .
    Create at least 1 task using either OSTaskCreate() or OSTaskCreateExt();
    .
    .
    OSStart();          /* Start multitasking!  OSStart() will not return */
}
```
### OSInit()

> OSInit() initializes all µC/OS-II variables and data structures (see OS_CORE.C).

在使用其他的 uC/OS service 之前，必須要先 call OSInit()。
OSInit() 將初始化所有 uC/OS-II 的 variable 和 data structure。

> OS_TASK_STAT_EN is set to 1,
> OS_FLAG_EN is set to 1,
> OS_LOWEST_PRIO is set to 63, and
> OS_MAX_TASKS is set to 62.

這些值是可以在 OS_CFG.H 設定的。

> After OSInit() has been called, the OS_TCB pool contains OS_MAX_TASKS entries. 
> The OS_EVENT pool contains OS_MAX_EVENTS entries, the OS_Q pool contains OS_MAX_QS entries, the OS_FLAG_GRP pool contains OS_MAX_FLAGS entries and finally, the OS_MEM pool contains OS_MAX_MEM_PART entries. 
> Each of the free pools are NULL pointer terminated to indicate the end. The pool is of course empty if any of the list pointers point to NULL. The size of these pools are defined by you in OS_CFG.H .

OSInit() 會建立 5 個 free pool:
* OS_TCB pool
* OS_EVENT pool
* OS_FLAG_GRP pool
* OS_Q pool
* OS_MEM pool

![](https://i.imgur.com/zAKEduR.png)

OSInit 進行系統初始化後建立的 variable 和 data structure如下：

![](https://i.imgur.com/nRTsqxl.png)

> 
> (1) You will notice that the task control blocks (OS_TCBs) of OS_TaskIdle() and OS_TaskStat() are chained together in a doubly linked list.
> 
> (2) OSTCBList points to the beginning of this chain. When a task is created, it is always placed at the beginning of the list. In other words, OSTCBList always points to the OS_TCB of last task created.
> 
> (3) Both ends of the doubly linked list point to NULL (i.e., 0).
> 
> (4) Because both tasks are ready to run, their corresponding bits in OSRdyTbl[] are set to 1. Also, because the bits of both tasks are on the same row in OSRdyTbl[], only one bit in OSRdyGrp is set to 1.

* OS_TCB list 是個 double link list 結構
* 新建立的 task 會加在 OS_TCB list 的 head




### OSStart()

```
void OSStart (void)
{
    INT8U y;
    INT8U x;
  
    if (OSRunning == OS_FALSE) {
        OS_SchedNew();                              
        OSPrioCur     = OSPrioHighRdy;
        OSTCBHighRdy  = OSTCBPrioTbl[OSPrioHighRdy];                       (1)
        OSTCBCur      = OSTCBHighRdy;
        OSStartHighRdy();                                                  (2)
    }
}
```
> (1) When called, OSStart() finds the OS_TCB (from the ready list) of the highest priority task that you have created.
> 
> (2) Then, OSStart() calls OSStartHighRdy() which is found in OS_CPU_A.ASM for the processor being used (see Chapter 13, Porting µC/OS-II). Basically, OSStartHighRdy() restores the CPU registers by popping them off the task’s stack then executes a return from interrupt instruction, which forces the CPU to execute your task’s code. Note that OSStartHighRdy() will never return to OSStart().


1. `OSStart()` 去找到目前被建立的 task 中 priority task 最高的 task
2. 呼叫 `OSStartHighRdy()` ，從 task stack 把指定 task 的 CPU content restore 後透過執行 return from interrupt instruction (ireq) 迫使 CPU 執行被指定的 task code
    * `OSStartHighRdy()` 被寫在 OS_CPU_A.ASM (Hardware dependent) 
    * `OSStartHighRdy()` 不會 return 回 `OSStart()`


OSStart 執行後的系統狀態：

![](https://i.imgur.com/3U66QNX.png)