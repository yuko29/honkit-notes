# Resource Synchronization

###### tags: `uC/OS` `RTOS`

## Reentrant Funcitons

又稱為 thread-safe functions，可安全地同時被多個 task 執行。

> The instances should share no states

The ways to make functions reentrant:
- Use local variables
- Use semaphore or mutex
- Disable interrupts / Enable interrupts

## Mutual Exclusion

The ways to enforce mutual exclusion

- Disabling interrupts
- The test-and-set instruction 
- Lock the scheduler
- A counting semaphore with init value 1
- A mutex

在 Non-real time systmes 中，Semaphore with init=1 和 Mutex 在功能上等價的，在 uC/OS-II 中有幾點不同：

- A mutex manages priority inheritance，a semaphore doesn't
- Mutex can only unlocked by its 



|                   | Disadv.                                    | Adv.             |
| ----------------- | ------------------------------------------ | -------------------- |
| Interrupt disabling | Affect tasks not using any resources  | Good for very short critical secitons  |
| Test-and-set instr. | 1. Waste of CPU cycles on uni-processor system <br> 2. Starvation is possible   | Atomic operation       |
| Scheduler diabling | 1. Affect tasks not using any resources <br> 2. ISR-task race exists | Better than interrupt disabling |
| Semaphores | 1. higher overheads <br> 2. µC/OS-II's semaphores do not manage priority inversion | 1. Good response <br> 2. Do not affect tasks that not involved in resource contention |
| Mutex |  | 1. Good response <br> 2. Priority inversion is managed  |


## Mutex in µC/OS-II

在 µC/OS-II 設計中，priority 必須是唯一的，意思是不論是 task 或是 mutex，其 priority 都必須不同。

µC/OS-II 對於 priority inheritance 問題的解決方法：保留 prioity 給 mutex 並且其 priority 必須要比其他使用這個 mutex 的 task 的 priority 還要高
- mutex 的 priority 只能比最高的 task priority 高一級，避免影響其他不相關的 task
- µC/OS-II 和 PIP 不同的地方：沒有 multiple/transitive priority 


## Mutex implementation details

- A task or an ISR signals a task through a kernel object called an Event Control Block (ECB).
- An ECB is used as a building block to implement services such as Semaphores and Mutex.
- µC/OS-II maintains the state of an ECB in a data structure called `OS_EVENT`.

```c
typedef struct {
    INT8U     OSEventType;                   /* Event type                        */
    void     *OSEventPtr;                    /* Ptr to message or queue structure */
    INT16U    OSEventCnt;                    /* Count (when event is a semaphore) */
    OS_PRIO   OSEventGrp;                    /* Group for wait list               */
    OS_PRIO   OSEventTbl[OS_EVENT_TBL_SIZE]; /* Wait list for event to occur      */
#if OS_EVENT_NAME_EN > 0u
    INT8U    *OSEventName;
#endif
} OS_EVENT;
```
以下討論 ECB 用於 Mutex 的實作

- `OSEventPtr`: 指向 mutex 的 owner 的 TCB
- `OSEventCnt`: 
    - upper 8 bit 為 mutex 的 priority
    - lower 8 bit 標示 mutex 是否 avaliable 或是 owner 的 priority

![](upload_58348ba54420f5c8c8c5b179648cf26a.png)

Waiting list 結構與 ready list 一樣都是 bitmap

![](upload_58348ba54420f5c8c8c5b179648cf26a.png)