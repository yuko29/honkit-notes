# Task Management

###### tags: `RTOS` `uC/OS`


## Create a Task

- 在 Multitasking 前 (呼叫 `OSStart()` 前) 必須至少建立一個 task
    - 呼叫 `OsInit()` and `OSStatInit()` 會建立 2 個 task (idle 和 stat)
- Necessary data structures for a new tasks
    - A Task Control Block (TCB)
    - A stack
    - An entry of the priority table
- Do not crate tasks in ISR 
    - task creation involves user-level context switch

```c
INT8U OSTaskCreate (void (*task)(void *pd), void *pdata, OS_STK *ptos, INT8U prio)
{
#if OS_CRITICAL_METHOD == 3
    OS_CPU_SR  cpu_sr;
#endif
    void      *psp;
    INT8U      err;
  
#if OS_ARG_CHK_EN > 0u
    if (prio > OS_LOWEST_PRIO) {                                           (1)
        return (OS_ERR_PRIO_INVALID);
    }
#endif
    OS_ENTER_CRITICAL();
    if (OSIntNesting > 0u) {                
        OS_EXIT_CRITICAL();
        return (OS_ERR_TASK_CREATE_ISR);
    }
 
    if (OSTCBPrioTbl[prio] == (OS_TCB *)0) {                               (2)
        OSTCBPrioTbl[prio] = (OS_TCB *)OS_TCB_RESERVED;                    (3)
        OS_EXIT_CRITICAL();                                                (4)
        psp = (void *)OSTaskStkInit(task, pdata, ptos, 0);                 (5)
        err = OS_TCBInit(prio, psp, (void *)0, 0, 0, (void *)0, 0);        (6)
        if (err == OS_ERR_NONE) {                                          (7)
            if (OSRunning == OS_TRUE) {                                    (8)
                OS_Sched();                                                (9)
            }
        } else {
            OS_ENTER_CRITICAL();
            OSTCBPrioTbl[prio] = (OS_TCB *)0;                             (10)
            OS_EXIT_CRITICAL();
        }
        return (err);
    }
    OS_EXIT_CRITICAL();
    return (OS_ERR_PRIO_EXIST);
}
```

(5) Stack initialization 是 hardward dependant 的
- CPU regs 不同
- Stack growing direction 不同（e.g., Toward "low" memory address in x86）

![](upload_0415e909c4b60d0671173b721f95b50f.png)

### Task Stack

The stack is a contiguous memory space，allocated from global arrays.

```c
OS_STK  MyTaskStack[stack_size];
```
The element size is hardware dependent.
Stack may grow upward or downward. (Depend on Arch)

## Delete a Task

Release all data structure associate with the deleted task.


```c
INT8U OSTaskDel (INT8U prio)
{
    1. Prevent from deleteing an idle task
    2. Prevent from deleteing a task from within an ISR
    3. Verify the task to be deleted does exist
    4. Remove the OS_TCB
    5. Remove the task from ready list or other list (e.g., mutex)
    6. Set .OSTCBStat to OS_STAT_RDY
    7. Call OSDummy()
    8. Call OSTaskDelHook()
    9. Remove the OS_TCB from priority table
    10.Call OSSched()
}

```

**第 6 點： 為何要將 `.OSTCBStat` 設為 `OS_STAT_RDY` ?**

Prevent the task from being resumed since we are not in the ready list now. (a "ready" task cannot be resumed)
> Note that OSTaskDel() is not trying to make the task ready, it is simply preventing another task or an ISR from resuming this task [i.e., in case the other task or ISR calls OSTaskResume()]. This situation could occur because OSTaskDel() will be re-enabling interrupts (see L4.10(12)), so an ISR can make a higher priority task ready, which could resume the task you are trying to delete. Instead of setting the task’s .OSTCBStat flag to OS_STAT_RDY, I simply could have cleared the OS_STAT_SUSPEND bit (which would have been clearer), but this takes slightly more processing time.
> 

**第 7 點：為何要 call OSDummy()?**

看一下上下文：
```c
    if (OSLockNesting < 255u) {                                           (11)
        OSLockNesting++;
    }
    OS_EXIT_CRITICAL();                                                   (12)
    OS_Dummy();                                                           (13)
    OS_ENTER_CRITICAL();                                   
    if (OSLockNesting > 0u) {                                             (14)
        OSLockNesting--;
    }
    OSTaskDelHook(ptcb);  
```
為了避免 interrupt latency 過長，所以途中會 enable interrupt 一次，再 disable interrupt，讓這中間 CPU 可以接受 interrupt 打進來（這段時間 scheduler 仍然是被 lock 住的）。

由於 CPU 硬體設計，當 enable interrupt 的指定下下去後，會迫使 CPU 直到下一個指令結束前，都還是維持在 interrupt disbled 狀態。再之後才會切換到 interrupt enabled。


> I do this because I want to make sure that the processor executes at least one instruction with interrupts enabled. On many processors, executing an interrupt enable instruction forces the CPU to have interrupts disabled until the end of the next instruction! The Intel 80x86 and Zilog Z-80 processors actually work like this. Enabling and immediately disabling interrupts would behave just as if I didn’t enable interrupts.

作者在這裡選擇 call `OS_Dummy()` 來確保在 re-diabling intterrupts 前執行了一個 call 和 return instruction。
`OS_Dummy()` 也可以改成 NOP。

> Calling OS_Dummy() thus ensures that I execute a call and a return instruction before re-disabling interrupts. You could certainly replace OS_Dummy() with a macro that executes a “no-operation” instruction and thus slightly reduce the execution time of OSTaskDel(). I didn’t think it was worth the effort of creating yet another macro that would require porting.

