# Introduction

Notes of NYCU RTOS course 2023 Spring

Link
---
- [uC/OS User Manual](https://micrium.atlassian.net/wiki/spaces/osiidoc/pages/163880/C+OS-II+User+Manual)
- [FreeRTOS-Kernel](https://github.com/FreeRTOS/FreeRTOS-Kernel)
- [成大資工 wiki FreeRTOS](https://wiki.csie.ncku.edu.tw/embedded/freertos)


Lab
---
- [FreeRTOS EDF](/64b4VHROSf6U0ok45m-psw)
- [yuko29/RTOS_lab](https://github.com/yuko29/RTOS_lab)
- [hcgcarry/RTOS_nctu](https://github.com/hcgcarry/RTOS_nctu)
- [Johnny-Ou/nctu_rtos](https://github.com/Johnny-Ou/nctu_rtos)