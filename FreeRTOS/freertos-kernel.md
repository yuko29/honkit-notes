# FreeRTOS Kernel

###### tags: `RTOS` `FreeRTOS`


FreeRTOS 中，Priority 數字越大越高。0 為最低的 priority。

## Task Ready List

FreeRTOS 中，task 可以有一樣的 priority。
Task Ready List 以 Multilevel round robin queue 實作。
Scheduler 會挑 priority 最高的 list，若有多個 task 在 list 上，則以 round robin 方式，以 tick 為時間單位輪流執行


![](upload_a9f4bb78707bffa661dfc917af01e7e2.png)

## Timer List

FreeRTOS 中，會把 delay task (sleeping task，又稱 timer)，集中成 `DelayedTaskList`，並會依照 timeout time (tick) 升序排列。
每個 tick 都比較 list 中第一個 timer 的 timeout time 和 current tick。
`DelayedTaskList` 會有兩個: `xDelayedTaskList1` 和 `xDelayedTaskList2`
- TickCount 不斷遞增下去會 overflow 變成 0
- 當計算 task 的 deadline 發現會 overflow 時，便將 task 排入另一個 list
- 當 TickCount 發生 overflow 了，就切換兩個 list，以此方式循環

In `tasks.c`

```c
PRIVILEGED_DATA static List_t xDelayedTaskList1;                         
/*< Delayed tasks. */
PRIVILEGED_DATA static List_t xDelayedTaskList2;                         
/*< Delayed tasks (two lists are used - one for delays that have overflowed the current tick count. */
```

```c
/* Start with pxDelayedTaskList using list1 and 
the pxOverflowDelayedTaskList using list2. */
pxDelayedTaskList = &xDelayedTaskList1;
pxOverflowDelayedTaskList = &xDelayedTaskList2;
```

```c
const TickType_t xConstTickCount = xTickCount + ( TickType_t ) 1;

/* Increment the RTOS tick, switching the delayed and overflowed
 * delayed lists if it wraps to 0. */
xTickCount = xConstTickCount;

if( xConstTickCount == ( TickType_t ) 0U ) /*lint !e774 'if' does not always evaluate to false as it is looking for an overflow. */
{
    taskSWITCH_DELAYED_LISTS();
}
```

![](upload_a9f4bb78707bffa661dfc917af01e7e2.png)


[FreeRTOS 之 Timer](https://zhuanlan.zhihu.com/p/336660464)
