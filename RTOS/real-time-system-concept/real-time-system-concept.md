# Real-Time System Concepts

###### tags: `RTOS` `uC/OS`

## Real-Time System

> A real-time system is a system whose correctness includes its response time as well as its functional correctness.
> 
> -- IEEE Definition on Real-Time Systems

簡單來說, 一個 system 能夠及時給出 reponse 且確保資訊的正確性，即為 real-time system。

## Embedded System
* 任務導向的 （Mission oriented）
* Highly resource-constrained
* 受制於 timing constraints

## Task

Task 構成：
* a priority
* a set of registers
* its own stack
* some housekeeping information (e.g., TCB)

5 state:
* Dormant
* Ready
* Reunning
* Waiting
* Interrupted

## Kernel

負責
* Task management
* Inter-task communication

kernel service take time

## uC/OS-II

* preemptive
* fixed-priority
* 可能發生 task-task race 和 task-ISR races

## Interrupt

Interrupt 有兩種：
* Hardware interrupt
    * async
    * clock tick, I/O event, hardware error
* Software-trigger interrupt
    * sync
    * context switch in uC/OS
    * divied by zero

Interrupt latency = 開始 interrupt disabling 到執行 ISR 第一條指令的時間

##  Interrupt Service Routine(ISR)

大部分 ISR 流程

1. Save the context of the current task
2. Recognize the interrupt
3. Obtain data or status form the interrupt device
4. Resume task execution

ISR 花時間，因此工作必須最小化，只做必須的事，完成後快速離開

### Clock Tick

timer 產生的週期性 hardware interrupt

tick rate 越高，context switch 頻率越高

## Memory Requirement

RAM usage = code + data + stack

* Code: 可在 compile time 決定 code size
* Data + stack
    * Global variables：包含 free list
        * 可在 compile time 決定
    * Task stack: used by ISR and kernel code
        * 部份可以在 offline analysis 被決定

Totla RAM requirement = application requirement + kernel requirement + SUM(each task(stack + MAX(ISR nesting))) (program call depth)

會佔用 stack 使用量的因素
* Large arrays and structures as local variable
* Recursive function call
* ISR nesting
* Function calls with many argument

