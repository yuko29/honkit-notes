# Resource Synchronization Protocols

###### tags: `RTOS` `uC/OS`

為了處理 resource contention 資源爭奪問題誕生。

## Introduction

資源 (a resource) 可被多個 task 共享，為了避免發生 race condition, deadlock，task 在存取 resource 必須透過同步機制，決定當下請求資源的 task 是否能馬上獲得，或是此請求必須被延遲。

> 資源請求是不會被拒絕的，只會被延遲，除非 time out

此同步機制必須確保等待時間是能控制（managable）且可被預期 (predictable) 的。

對於 Non-preemptible / cyclic executive / frame-based scheduling 不需要 resource sync protocol。
亦即只有會發生 preemptible scheduling 需要。

### Problem

像是 mutex 一樣，job 必須取得該資源的 lock 才能使用該資源，使用完後必須 unlock 它。

當 job 想要存取資源時，它會試圖 lock 該資源。若該資源當下有其他 job 在使用，此請求就會被延遲。若當下在使用該資源的是 lower priority 的 job，此現象稱為 blocked: The task is blocked by the lower prioity task。
Resource contention 會增加 task response time。

:::info
block: 要用的 resource 被 low priority 的 task 佔用而導致 task 無法繼續執行。
:::

> job == task

:::warning
Locking 和 unlocking 後都可能會發生 context switch
:::

> task A, B; resource R。A, B 都要用 R，A 佔用 R，prio: A < B
> * Locking: 等待 lock 的空檔可能會被其他高優先度的 task 搶佔
> * Unlocking: 當 A 釋放 R 時，B 等到 R 後搶佔 A 

![](oB0OFDb.png)

## Undesirable Effects

### 1. Priority inversion

高優先度的 job 會因為低優先度的 job 的干涉而不能執行。
亦即：高優先度的 job 試圖要 lock resource 但它正被低優先度的 job lock 住。

> Priority inversion is the phenomenon where a higher priority job is
blocked by lower priority jobs.

> A low-priority job never be "blocked" by high-priority jobs.
> 
> Task 要 lock resource 時，如果有更高優先度的 task 要 lock 該 resource，高優先度的 task 應會先於該 task 執行且先於該 task loce resource，因此前提不成立。

下圖：J2 間接的 block 了 J1，即使 J1 和 J2 當前並沒有在搶奪同一個資源

> J1 實際上是被 J3 block 住，但是 J2 preempt 了 J3，間接 block J1，可能導致 J1 violate deadline。
> 此為 **uncontrollable priority inversion**

![](gGuJvBb2.png)




### 2. Timing anomalies

理想上，如果 job 比預期中早完成，其他 job 的 response time 理應也要提早。但因為 resource contention 的問題，這件事可能不成立。

![](K0GUUxc3.png)


### 3. Deadlock

job 之間互相等待對方佔有資源的 lock 造成 circular waiting。 

![](Xi5jvQG4.png)


## Protocols

在只有一個 job 可以佔用 resource 的情況下（mutual-exclusive access），Priority inversion 和 timing anomalies 是不可避免的。於是必須要控制、限制這兩個 effect 的影響時長。

而 Deadlock 發生後不可解，是必須要避免的。

A set of rules:

- To grant nd to postpone lock requests
- To schedule jobs (priority manipulation)

### Non-Preemptible Critical Section (NPCS)

> [Mok] NPCS: A job becomes non-preemptible when it is using a resource.

當該 job 佔有 reosurce 時，其他的 job 都不能搶佔它。

- 可完全避免 Deadlock
- 還是會發生 priority inversion

![](c9FuxF45.png)


![](5IYkDQZ6.png)


Pros:

* No need to know resource usage

Cons:

* Poor response of high prioirty tasks
* An LPT always blocks a HPT even if they do not share any resources

### Ceiling-Priority Protocol (CPP)

A task set $T = \{T_a, T_b, T_c, T_d, T_e, ...\}$, sorted by priority (high -> low)
$T_b, T_d, T_e$ share a resource R.

當有個 task 要 **lock** R 時，把該 task 的 priority 上升到 $T_b$ 的 priority，直到 unlock R。

* 又稱 highest-locker protocol
* Better response than NPCS
* 可避免 uncontrollable priority inversion 和 deadlock


![](AL2YbMN7.png)



### Priority-Inheritance Protocol (PIP)

> NPCS 和 CPP 都會遇到一個問題： uncontrollable priority inversion 仍會發生，CPP 比 NPCS 好一些
> 為了解決 uncontrollable priority inversion 而發明

與 CPP 不同之處： priority boosting 時機不同

當 the requesting job $J$ 被 **block** 時，block $J$ 的 job $J_l$ 的 priority 要升至與當下 $J$ 相同。


#### Transitive blocking

Trasitive priority inheritance 會造成 Transitive blocking


![](rkN6twJrn.png)

#### Chain blocking

> Example
> 
> 如下圖, R1(綠), R2(紅)
> 
> $J_L$ lock 了 R1，$J_M$ preempt $J_L$ lock 了 R2，而 $J_H$ 後來抵達，需要依序存取 R1 和 R2 時，需要等待兩個 critical section：第一個是等待 $J_L$ 釋放 R1，第二個是等待 $J_M$ 釋放 R2。

在 chain blocking 下，task 可能會被 block 多個 critical section

![](Hk658dySn.png)




Pros:

* Better response time than NPCS & CPP
    * high priority task 在被 block 之前不會被 priority risen 影響到，仍可執行 task，相較 CPP 受影響較小
* 不會有 uncontrollable priority inversion 發生

Cons:

* Multiple blocking (bounded): Chain blocking & transitive blocking
    * 雖然 blocking duration 是 bounded 的，仍可能會很長
* 可能發生 deadlock

### Priority-Ceiling Protocol (PCP)

> PIP 遺留了 chain blocking 和 deadlock 問題
> PCP 主要的目的就是防止這兩件事情發生

* 禁止 chain/transitive blocking
    * 減少 the worst case blocking time
    * task 最多只會被 block 1 critical section
* 禁止 circular waiting
    * 解 deadlock

定義 

* $\Pi(R)$: Priority ceiling of resource R
    * 需要 R 的 tasks 中最高的 priority 
* $\hat\Pi(t)$: System ceiling
    * 考慮當下時間 t 所被使用的 resource 中，$\Pi(R)$ 最高的值
* $\pi(t)$: job 在 t 時間點的 priority

引入 system ceiling 概念，即便 resource 已經 ready， task 的 $\pi(t)$ > $\hat \Pi(t)$ 才可以有資格拿 resource，否則就要等。




![](SkbTpu1B2.png)


* 新的 blocking 情境：Blocked by ceiling
    * PCP 中，即使所要的 resource 已經 ready，但可能會因為 task priority 沒超過 system ceiling 導致 request 被延後
* PCP 的 the worst-case blocking time 不會長於 PIP
    * PCP 下不會有 trasitice/chain blocking
* PCP 下，task 只會被 block 一次
    * 當 HPT 已經拿到 R 後，不會有其他 LPT 還拿著 HPT 需要的資源，否則 HPT 就沒辦法拿到 R
    * 假設有其他的 LPT 還拿著 HPT 需要的資源 R'，那麼 system ceiling 就應該會 >= HPT 的 priority，如此一來 HPT 是無法取得 R 的
* PCP 中 system ciling 機制阻斷 circular waiting
* Blocking Time
    * 對於有要求資源的 task 來說，PIP 和 PCP 下的 blocking time 是相同的
    * 對於沒使用任何資源的 task 來說，在 PCP 下則不會被 ceiling blocking

#### Schedulable test


![](B1BHMoJSn.png)

task 的 response time = Preempt time from HPT + blocking time from LPT + its task execution time

如果考慮到 context switch overhead，
* preempting job 會有頭尾兩個 context switch 
* 考慮 blocking 會再加上兩個 context switch

![](H1soxiyS3.png)

$f(j)$ = 4 if blocking
$f(j)$ = 2 if no blocking

:::info
PCP 為 POSIX 標準
:::

### Stack-Resource Policy (SRP)

> 將 PCP+RM 排程概念 retarget 到 EDF 上

EDF 中沒有 task priority 的概念，因此將 priority 替換成了 preemption level 

preemption level 以 task 的 period 長度衡量，越短 preemption level 越高

* preemption level -> for ceiling
* Deadlines -> for inheritance

Schedule rule: task 必須要等到 preemption level 高於 current system ceiling 才能執行

> 與 PCP 不同之處
> 
> * PCP: check system ceiling on locking
> * SRP: check system ceiling on scheduling


![](BkKOLiyH2.png)

特點

* Last-In First-Out
* Deadlock avoidance
    * 當一個 task 開始執行時，其所需要的 resource 都已經被其他 task 釋放

Pros:

* 所有的 task 可以共享一個 stack
* 省下 2 個 context switch
* 好實作

Cons:

* HPT 的 response time 相較 PCP 差

![](ryoD9j1Sh.jpg)




