# Independent Task Scheduling

![](upload_fbc6596ddcc2b54758e9da3fcdc41721.png)


## Cyclic Executive

Table-driven approach

- A super loop，定好每個 iteration 中 task 在什麼時間執行，時間表的概念
- 優點：易於 debug, visualize, highly deterministic
- 缺點：要增減 task 都需要大幅度更動 schedule

## Rate-Monotonic Scheduling (RMS)
 
- Fixed-Priority Scheduling
- A task set={$T_{1}, T_{2}, ... , T_{n}$} is schedulable by RM $\Leftrightarrow$ the worst-case response times of every task are shorter than their periods
- Period 越短的 task，priority 越高
- Preemptive


### Rate-Monotonic Analysis (RMA)

- Response time analysis, recursive algorithm
- 計算每個 task 的 critical instance 的 worst response time
- 每個 task 的 critical instance 的 worst response time 必須要 meet deadline
- 每個 task 都必須要通過測試

![](upload_5f41578efbba9b5bf21c1830f32f926b.png)

- Time Complexity
    - $O(n^{2} * p_n)$ , pseudo-polynomial time
    - 當 task periods 是可被互相整除的情況下可以很快，但如果是互質的話則會非常慢
    - 因為計算慢所以在 dynamic system 中 RMA 不常被使用 (計算複雜度小於 polynomial time 才適合)
    - 適合 static system, offline analysis
- 可適用於 arbitrary task priority assignment


### Sufficient Test： Utilization test

- [LL73] A task set { $T_{1}, T_{2}, ... , T_{n}$ } is schedulable by RMS if　$$\Sigma^{n}_{i=1}\frac{c_{i}}{p_{1}} \leq U(n) = n(2^{\frac{1}{n}}-1)$$
- 計算 task set 的 CPU utilization，小於 the lowest CPU utilization 則保證一定可以 scheduled by RM
- 當 n 趨近無限大，U(n) 趨近 0.68
- Time Complexity: $O(n)$
- 因為是 sufficient condition，通過這個 test 的 task set 表示可 scheduled by RM，**但沒通過的 task set 有可能也可以用 RM schedule**，或是無法被 RM schedule
- 只可用於 RM

### Optimality of RM

Arbitrary task priority assignment 
- Utilization test 不可用 （只能用於 RM）
- RMA 可用
- 可以藉由 swap task priority 把問題 reduce 成 RM $\Rightarrow$ RM is optimal


## Earliest Deadline First (EDF)

- Feasible
    - $\Sigma \frac{c}{p} \leq 100 \%$ $\Rightarrow$ 存在某種工作排程方式可以不 miss 任何 deadline
- EDF is universal to periodic, preemptive tasks
    - Universal scheduling algorithm 定義
	    - Schedulable $\Leftrightarrow$ feasible
- 永遠先執行 deadline 最近的 task
- With EDF, there is no idle time before an overflow

### Optimality to EDF



|     | periodic | non-periodic |
| ---------- | -------- | ------------ |
| preemptive | ?(Universal) [Mok] | O [Horn's Rule] |
| non-preemptive  | X (NPC) [Jeffay] |   O [Jackson's Rule]   |


![](upload_bd1140f9f4a87588dbd9a0743baddf6b.png)

## Context-Switch Overhead

A task always preempts and resumes to the same task.

在 Execution time 頭尾加 2 個 context switch overhead.

![](upload_808f726a9c1a6c2fb6d9b2d4b72aae62.png)


EDF 和 RM 一樣都加 2 個 CS overhead

## Harmonically-related tasks

Harmornic chain: 指的是一個 task set，裡面的 task 都可以被整除 task 中最長的 period

e.g. $T = \{(1, 4), (1, 8), (1, 7), (1, 16)\}$

原本在 utilization test 中，需使用 U(4)
但是其中 4, 8 可以整除 16，因此整個 task set 可以寫成
$\{(1+0.5+0.25, 4), (1, 7)\}$，便能使用 U(2)


## Cycle-Based Scheduling

- 每個 task 的 period 是相同的 => frame

